/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Request handler for Peer server
 */
public class PeerServerHandler implements RequestHandler {

    /**
     * Returns the handled methods
     *
     * @return array of methods
     */
    public String[] handledRequests() {
        return new String[]{"hi", "hello", "ping", "lookup",
                "store_c", "store_p", "get"};
    }

    /**
     * Processes the requests and sends response
     *
     * @param request        RPC request
     * @param messageContext msg context
     * @return response
     */
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {

        String method = request.getMethod();

        switch (method) {
            case "lookup":
                PeerInfo peer = handleLookup(request.getNamedParams());
                String result = (peer == null) ? null : peer.id + "," + peer.ip;
                return new JSONRPC2Response(result, request.getID());
            case "store_c":
                Map<String, Object> params = request.getNamedParams();
                long clientID = (long) params.get("clientID");
                String msg = (String) params.get("msg");
                int res = handleStoreFromClient((int) clientID, msg);

                return new JSONRPC2Response(res, request.getID());
            case "store_p":
                handleStoreFromPeer(request.getNamedParams());
                return new JSONRPC2Response(true, request.getID());
            case "get":
                msg = handleGet(request.getNamedParams());
                return new JSONRPC2Response(msg, request.getID());
            case "ping":
                return new JSONRPC2Response(Config.nodeID + "," + Config.ip, request.getID());
            case "hi":
                return new JSONRPC2Response("hello", request.getID());
            case "hello":
                return new JSONRPC2Response("hi", request.getID());
            default:
                return null;
        }
    }

    /**
     * Handles Store request from client
     *
     * @param clientID client id
     * @param s        message
     * @return key
     */
    private int handleStoreFromClient(int clientID, String s) {
        int key = (s.hashCode() & Integer.MAX_VALUE) % Config.maxNodes;
        // if key == node id, store it
        if (key == Config.nodeID) {
            KademliaPeer.store(clientID, key, s);
            return key;
        }
        System.out.printf("Received message {%d: %s} from client %d\n", key, s, clientID);

        // named params for sending additional info
        Map<String, Object> params = new HashMap<>();
        params.put("node", (long) key);
        params.put("from_id", (long) Config.nodeID);
        params.put("from_ip", Config.ip);

        // lookup
        PeerInfo closest = handleLookup(params);

        if (closest == null) {
            // get closest node from available peers
            closest = KademliaPeer.getClosest(key);

            // if this peer is the closest, store the data here
            if (closest.id == Config.nodeID) {
                KademliaPeer.store(clientID, key, s);
                return key;

                // else send the data to the closest node
            } else {
                if (PeerClient.storeFromPeer(clientID, key, s, closest)) {
                    return key;
                }
            }
            // when closest is not null, store there.
        } else {
            if (PeerClient.storeFromPeer(clientID, key, s, closest)) {
                return key;
            }
        }

        return -1;
    }

    /**
     * Handles Store requests from peers
     *
     * @param params
     */
    private void handleStoreFromPeer(Map<String, Object> params) {
        long key = (long) params.get("key");
        long clientID = (long) params.get("clientID");
        String message = (String) params.get("msg");

        KademliaPeer.store((int) clientID, (int) key, message);
    }

    /**
     * Handles GET requests
     *
     * @param params named params
     * @return message/null
     */
    private String handleGet(Map<String, Object> params) {
        long key = (long) params.get("key");
        long clientID = (long) params.get("clientID");

        System.out.printf("GET from %d for key: %d\n", clientID, key);
        String result = KademliaPeer.get((int) key, (int) clientID);
        if (result != null) {
            System.out.printf("Returning result: %s\n", result);
            return result;
        } else {
            PeerInfo closest = KademliaPeer.getClosest((int) key);
            if (Config.nodeID == closest.id) {
                result = KademliaPeer.get((int) key, (int) clientID);
                System.out.printf("Returning result: %s\n", result);
                return result;
            }
            System.out.printf("Forwarding GET %d to node %d\n", key, closest.id);
            return PeerClient.getFromPeer((int) key, closest.ip, (int) clientID);
        }
    }

    /**
     * Handles Lookup requests
     *
     * @param params named params
     * @return closest peers with longest matching prefix
     */
    private PeerInfo handleLookup(Map<String, Object> params) {
        long lookupNode = (long) params.get("node");
        long peerID = (long) params.get("from_id");
        String peerIP = (String) params.get("from_ip");

        PeerInfo peer = new PeerInfo((int) peerID, peerIP);
        if (Config.verbose)
            System.out.printf("Peer %d requested to lookup %d \t", peerID, lookupNode);

        PeerInfo closest = RoutingTable.getClosest((int) lookupNode);
        if (Config.verbose)
            System.out.println("Responding: " + closest);
        RoutingTable.insert(peer);

        return closest;
    }
}

