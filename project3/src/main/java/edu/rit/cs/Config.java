/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Configuration class
 */
public class Config {
    // ip of the node is automatically assigned
    public static String ip = null;
    // node id is assigned from program args
    public static int nodeID = 12;
    // server port is fixed for each peer
    public static int port = 40_000;
    // bootstrap ip is required in the initial stages
    // constructed from the program args
    public static String bootstrapIP = null;
    // maximum nodes in the DHT. Can be any value
    public static int maxNodes = 16;
    // used like a log level from console
    public static volatile boolean verbose = false;

    static {
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
