/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implements a Kademlia Peer
 */
public class KademliaPeer {

    // stores the peer info in a hash table for faster access.
    private static ConcurrentHashMap<Integer, String> peers;
    // {key: {clientID: data}}
    private static DataStore data;
    // set of all keys stored in the data store
    private static HashSet<Integer> keyStore;

    /**
     * Constructor of the kademlia peer.
     * Initializes the data and routing table and starts the
     * bootstrapping process.
     */
    public KademliaPeer() {
        peers = new ConcurrentHashMap<>();
//        data = new HashMap<>();
        data = new DataStore();
        keyStore = new HashSet<>();
        RoutingTable routingTable = new RoutingTable();

        if (!Config.ip.equals(Config.bootstrapIP)) {
            lookupPeer(Config.nodeID, Config.bootstrapIP);
        }
    }

    /**
     * Inserts key, clientID and value to the data store
     *
     * @param clientID client id
     * @param key      key
     * @param value    message
     */
    public static void store(int clientID, int key, String value) {
        System.out.printf("Storing {%d: %s} from client %d \n", key, value, clientID);
        data.insert(key, clientID, value);
    }

    /**
     * Gets the value for a key
     *
     * @param key      key
     * @param clientID client id
     * @return message
     */
    public static String get(int key, int clientID) {
        return data.get(key, clientID);
    }

    /**
     * Adds a new peer to peers
     *
     * @param id peer id
     * @param ip peer ip
     */
    public static void addPeer(int id, String ip) {
        peers.put(id, ip);
    }

    /**
     * Removes a peer from peers
     *
     * @param key peer id
     */
    public static void removeFromPeers(int key) {
        peers.remove(key);
    }

    /**
     * Adds a key to the key store
     *
     * @param key key
     */
    public static void addKey(int key) {
        keyStore.add(key);
    }

    /**
     * Get the closest available peer from n
     *
     * @param n peer id
     * @return closest peer
     */
    public static PeerInfo getClosest(int n) {
        int closest = Config.nodeID;
        int d = closest ^ n;
        for (int p : peers.keySet()) {
            if ((p ^ n) < d) {
                closest = p;
                d = closest ^ n;
            }
        }
        if (closest == Config.nodeID) return new PeerInfo(Config.nodeID, Config.ip);
        return new PeerInfo(closest, peers.get(closest));
    }

    /**
     * Get closest available peer
     *
     * @param n peer id
     * @return closest peer id
     */
    private static int getClosestAvailableNode(int n) {
        int result = Integer.MAX_VALUE;
        int d = result ^ n;

        for (int p : peers.keySet()) {
            if ((n ^ p) < d) {
                result = p;
                d = n ^ p;
            }
        }

        return result;
    }

    /**
     * Refresh the routing table by looking for random peer ids
     * and looking up the random id from the closest available neighbor
     * to the random id.
     */
    public void refresh() {
        while (true) {
            // generate a random unknown peer id bounded by the max nodes
            Random random = new Random();
            int n = random.nextInt(Config.maxNodes);
            if (n == Config.nodeID || peers.containsKey(n)) continue;

            int exploreNode = getClosestAvailableNode(n);
            if (exploreNode < Config.maxNodes) {
                // lookup the random node
                lookupPeer(n, peers.get(exploreNode));
            } else continue;

            // sleep
            try {
                Thread.sleep(10_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Check if the peers in the routing table are still online
     */
    public void checkPeers() {
        while (true) {
            for (int peerID : peers.keySet()) {
                String peerIP = peers.get(peerID);
                // ping peer
                PeerInfo peer = PeerClient.isPeerOnline(peerIP);
                if (peer == null) {
                    // remove if not online
                    removePeer(peerID);
                    break;
                }
            }

            // sleep
            try {
                Thread.sleep(5_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Remove a peer from the routing table and peers
     *
     * @param peerID peer id
     */
    private void removePeer(int peerID) {
        peers.remove(peerID);
        RoutingTable.remove(peerID);
        System.out.println("Removed peer: " + peerID);
    }

    /**
     * lookup a peer
     *
     * @param node peer id
     * @param ip   peer ip
     */
    public void lookupPeer(int node, String ip) {
        if (ip.equals(Config.ip)) return;
        PeerInfo bootNode = PeerClient.isPeerOnline(ip);
        if (bootNode != null) {
            RoutingTable.insert(bootNode);
        }

        if (Config.verbose)
            System.out.println("Looking up " + node + " from " + ip);
        PeerInfo returned = PeerClient.lookup(node, ip);
        while (returned != null && returned.id != Config.nodeID) {
            if (Config.verbose)
                System.out.println("Returned peer: " + returned);
            RoutingTable.insert(returned);
            if (Config.verbose)
                System.out.println("Looking up " + node + " from " + returned.ip);
            returned = PeerClient.lookup(node, returned.ip);
        }
    }

    /**
     * Show commands on STDOUT
     */
    public void commands() {
        String commands =
                "1. Show routing and data tables\n" +
                        "2. Toggle verbose\n" +
                        "3. EXIT";

        try {
            Scanner sc = new Scanner(System.in);
            String commandNum = "";

            while (!"3".equals(commandNum)) {
                System.out.println("\n========  Choose a command =========");
                System.out.println(commands);
                commandNum = sc.next();

                switch (commandNum) {
                    case "1":
                        RoutingTable.print();
                        data.print();
                        break;
                    case "2":
                        boolean temp = Config.verbose;
                        Config.verbose = !temp;
                        break;
                    default:
                        break;
                }
            }

            // for all keys in the data store
            for (int key : keyStore) {
                int closest = getClosestAvailableNode(key);

                // get data from the data store
                if (closest >= 0 && closest < Config.maxNodes && closest != Config.nodeID) {
                    HashMap<Integer, String> contents = DataStore.getAll(key).get(key);

                    // send data to the closest peer to store
                    for (int clientID : contents.keySet()) {
                        PeerClient.storeFromPeer(clientID, key, contents.get(clientID),
                                new PeerInfo(closest, peers.get(closest)));
                    }
                }
            }

            System.out.println("Exiting program");
            System.exit(-2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

