/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

/**
 * This is the driver class which takes program arguments and
 * starts the execution
 */
public class Driver {

    public static void main(String[] args) {

        if (args.length != 3) {
            System.out.println("Usage: java Driver node id, bootstrap ip, P/C");
            System.exit(-1);
        }

        Config.nodeID = Integer.parseInt(args[0]);
        Config.bootstrapIP = "172.18.0." + args[1];

        // if starting a peer
        if ("P".equals(args[2])) {
            // start the RPC server
            new Thread(() -> new PeerServer().startServer()).start();
            // build skeleton routing and data tables and start the
            // bootstrapping process
            KademliaPeer kademliaPeer = new KademliaPeer();
            // start a thread to keep refreshing the table
            new Thread(() -> kademliaPeer.refresh()).start();
            // start a thread to remove inactive peers
            new Thread(() -> kademliaPeer.checkPeers()).start();
            // run the commands to take arguments from STDIN
            kademliaPeer.commands();

            // starting a client
        } else {
            new Client().commands();
        }
    }
}
