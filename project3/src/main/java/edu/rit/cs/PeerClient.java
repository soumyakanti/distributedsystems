/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Random;

/**
 * This class has helper methods to send RPC messages to the server
 */
public class PeerClient {

    /**
     * Creates a new session
     *
     * @param ip server ip
     * @return new session
     */
    private static JSONRPC2Session getSession(String ip) {
        URL serverUrl = null;
        try {
            serverUrl = new URL("http://" + ip + ":" + Config.port);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        assert serverUrl != null;
        return new JSONRPC2Session(serverUrl);
    }

    /**
     * Pings the peer to see if it's online
     *
     * @param ip server ip
     * @return null if peer not online, else info of the peer
     */
    public static PeerInfo isPeerOnline(String ip) {
        JSONRPC2Session session = getSession(ip);
        JSONRPC2Request req = new JSONRPC2Request("ping", new Random().nextInt());
        JSONRPC2Response response = null;

        try {
            response = session.send(req);
        } catch (JSONRPC2SessionException e) {
//            e.printStackTrace();
        }

        if (response != null && response.indicatesSuccess()) {
            if (response.getResult() == null) return null;
            //[id, ip]
            String[] result = response.getResult().toString().split(",");
            return new PeerInfo(Integer.parseInt(result[0]), result[1]);
        } else {
            return null;
        }
    }

    /**
     * Lookup a peer by messaging another peer. Returns the closest
     * peer available.
     *
     * @param node peer id
     * @param ip   server ip
     * @return closest peer
     */
    public static PeerInfo lookup(int node, String ip) {
        JSONRPC2Session session = getSession(ip);
        JSONRPC2Request req = new JSONRPC2Request("lookup", new Random().nextInt());

        // add named params to send
        HashMap<String, Object> params = new HashMap<>();
        params.put("node", node);
        // info of the sending peer so that the receiving peer can
        // add these info to its routing table
        params.put("from_id", Config.nodeID);
        params.put("from_ip", Config.ip);

        req.setNamedParams(params);

        JSONRPC2Response response = null;

        try {
            response = session.send(req);
        } catch (JSONRPC2SessionException e) {
//            e.printStackTrace();
        }

        if (response != null && response.indicatesSuccess()) {
            if (response.getResult() == null) return null;
            //[id, ip]
            String[] result = response.getResult().toString().split(",");
            return new PeerInfo(Integer.parseInt(result[0]), result[1]);
        } else {
            return null;
        }
    }

    /**
     * This method is used to give a Store command to a peer from a peer
     *
     * @param clientID client id
     * @param key      key
     * @param message  message
     * @param peer     receiving peer
     * @return boolean
     */
    public static boolean storeFromPeer(int clientID, int key, String message, PeerInfo peer) {
        String peerIP = peer.ip;
        int peerID = peer.id;
        System.out.printf("Sending message {%d: %s} to %d to store\n", key, message, peerID);
        JSONRPC2Session session = getSession(peerIP);
        JSONRPC2Request req = new JSONRPC2Request("store_p", new Random().nextInt());

        // add required parameters to the RPC message
        HashMap<String, Object> params = new HashMap<>();
        params.put("key", key);
        params.put("msg", message);
        params.put("clientID", clientID);

        req.setNamedParams(params);

        JSONRPC2Response response = null;

        try {
            response = session.send(req);
        } catch (JSONRPC2SessionException e) {
//            e.printStackTrace();
        }

        return response != null && response.indicatesSuccess();
    }

    /**
     * This method is used to give the Store command from a client to a peer
     *
     * @param message message
     * @param ip      ip of the peer
     * @return key
     */
    public static int storeFromClient(String message, String ip) {
        JSONRPC2Session session = getSession(ip);
        JSONRPC2Request req = new JSONRPC2Request("store_c", new Random().nextInt());

        HashMap<String, Object> params = new HashMap<>();
        params.put("msg", message);
        params.put("clientID", Config.nodeID);

        req.setNamedParams(params);

        JSONRPC2Response response = null;

        try {
            response = session.send(req);
        } catch (JSONRPC2SessionException e) {
//            e.printStackTrace();
        }

        if (response != null && response.indicatesSuccess()) {
            if (response.getResult() == null) return -1;
            long result = (long) response.getResult();

            return (int) result;
        } else {
            return -1;
        }
    }

    /**
     * This method is used to implement the GET functionality for client
     *
     * @param key key
     * @param ip  peer ip
     * @return message/null
     */
    public static String getFromClient(int key, String ip) {
        JSONRPC2Session session = getSession(ip);
        JSONRPC2Request req = new JSONRPC2Request("get", new Random().nextInt());

        HashMap<String, Object> params = new HashMap<>();
        params.put("clientID", Config.nodeID);
        params.put("key", key);

        req.setNamedParams(params);

        JSONRPC2Response response = null;

        try {
            response = session.send(req);
        } catch (JSONRPC2SessionException e) {
//            e.printStackTrace();
        }

        if (response != null && response.indicatesSuccess()) {
            return (String) response.getResult();
        }

        return null;
    }

    /**
     * Implements the GET method for a peer
     *
     * @param key      key
     * @param ip       peer ip
     * @param clientID client id
     * @return message
     */
    public static String getFromPeer(int key, String ip, int clientID) {
        JSONRPC2Session session = getSession(ip);
        JSONRPC2Request req = new JSONRPC2Request("get", new Random().nextInt());

        HashMap<String, Object> params = new HashMap<>();
        params.put("clientID", clientID);
        params.put("key", key);

        req.setNamedParams(params);

        JSONRPC2Response response = null;

        try {
            response = session.send(req);
        } catch (JSONRPC2SessionException e) {
//            e.printStackTrace();
        }

        if (response != null && response.indicatesSuccess()) {
            return (String) response.getResult();
        }

        return null;
    }


    public static void main(String[] args) {
        System.out.println(isPeerOnline("127.0.0.1"));
        lookup(2, "127.0.0.1");
    }
}
