/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

/**
 * Class to store peer information
 */
public class PeerInfo {
    int id;
    String ip;

    /**
     * Constructor for peer info
     *
     * @param id peer id
     * @param ip peer ip
     */
    public PeerInfo(int id, String ip) {
        this.id = id;
        this.ip = ip;
    }

    public String toString() {
        return String.format("Peer{id: %d, ip: %s}", id, ip);
    }
}
