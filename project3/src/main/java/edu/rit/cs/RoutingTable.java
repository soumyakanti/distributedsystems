/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;


/**
 * Implements a routing table for a Kademlia peer, using Trie data structure.
 */
public class RoutingTable {
    static Node root;

    /**
     * Constructor for the class. Also builds the skeleton routing table
     * and prints it
     */
    public RoutingTable() {
        root = new Node();
        build();
        print();
    }

    /**
     * Utility function to get binary string representation of an integer.
     *
     * @param n any integer
     * @return StringBuilder
     */
    public static StringBuilder getNodeString(int n) {
        int maxLength = Integer.toBinaryString(Config.maxNodes - 1).length();
        StringBuilder nodeString = new StringBuilder(Integer.toBinaryString(n));
        while (nodeString.length() < maxLength) {
            nodeString.insert(0, "0");
        }

        return nodeString;
    }

    /**
     * Inserts a peer to the routing table. Also moves data to the added
     * peer if this peer has keys which are closer to the added peer.
     *
     * @param node peer to be added
     */
    public static void insert(PeerInfo node) {
        StringBuilder nodeString = getNodeString(node.id);
        root.insert(node.id, nodeString.toString(), node.ip);

        // move data to the added peer if the keys are closer
        //       key:           clientID: message
        HashMap<Integer, HashMap<Integer, String>> data = DataStore.getAll(node.id);

        if (data != null) {
            for (int key : data.keySet()) {
                HashMap<Integer, String> contents = data.get(key);
                if (contents == null) break;
                for (int clientID : contents.keySet()) {
                    PeerClient.storeFromPeer(clientID, key, contents.get(clientID), node);
                }
            }
        }
    }

    /**
     * Remove a peer from the routing table.
     *
     * @param peerID peer id
     */
    public static void remove(int peerID) {
        StringBuilder nodeString = getNodeString(peerID);
        root.remove(nodeString.toString());
    }

    /**
     * Get the closest peer to the peer id
     *
     * @param n peer id
     * @return Closest peer
     */
    public static PeerInfo getClosest(int n) {
        StringBuilder nodeString = getNodeString(n);
        return root.getClosest(nodeString.toString());
    }

    /**
     * Print the routing table
     */
    public static void print() {
        root.print();
    }

    /**
     * Main method for testing
     *
     * @param args args
     */
    public static void main(String[] args) {
        RoutingTable table = new RoutingTable();
        table.insert(new PeerInfo(11, ""));
    }

    /**
     * Builds the skeleton routing table, without inserting any peer.
     */
    private void build() {
        StringBuilder nodeString = getNodeString(Config.nodeID);

        while (nodeString.length() > 0) {
            // get the last char
            int i = nodeString.length() - 1;
            int last = Character.getNumericValue(nodeString.charAt(i));

            // flip the last char
            nodeString.setCharAt(nodeString.length() - 1,
                    Character.forDigit(last ^ 1, 10));

            // build the particular prefix path
            root.buildPath(nodeString.toString());

            // delete the last character
            nodeString.deleteCharAt(i);
        }
    }

}


/**
 * Node represents a Trie node, containing a peer and Map of children.
 */
class Node {
    PeerInfo peer = null;
    HashMap<Character, Node> next = new HashMap<>();

    /**
     * Builds a particular prefix path
     *
     * @param s prefix string
     */
    public void buildPath(String s) {
        Node curr = this;
        for (char c : s.toCharArray()) {
            if (!curr.next.containsKey(c)) {
                curr.next.put(c, new Node());
            }

            curr = curr.next.get(c);
        }
    }

    /**
     * Returns true if the node is a leaf, else false.
     *
     * @return boolean
     */
    public boolean isLeaf() {
        return next.size() == 0;
    }


    /**
     * Inserts a peer into the Trie.
     *
     * @param n  peer id
     * @param s  peer id in binary
     * @param ip peer ip
     */
    public void insert(int n, String s, String ip) {
        if (n == Config.nodeID) return;
        Node curr = this;

        // traverse the prefix
        for (char c : s.toCharArray()) {
            // if leaf is encountered, add the new node only if the peer is closer than the
            // existing peer
            if (curr.isLeaf()) {
                if (curr.peer == null || (curr.peer.id != n && closer(curr.peer.id, n) == n)) {
                    if (curr.peer != null) {
                        KademliaPeer.removeFromPeers(curr.peer.id);
                    }
                    curr.peer = new PeerInfo(n, ip);
                    KademliaPeer.addPeer(n, ip);
                    System.out.printf("Added peer %d with ip %s \n", n, ip);
                }
                return;
            } else {
                curr = curr.next.get(c);
            }
        }

        //add the new node only if the peer is closer than the
        //existing peer
        if (curr.isLeaf()) {
            if (curr.peer == null || (curr.peer.id != n && closer(curr.peer.id, n) == n)) {
                if (curr.peer != null) {
                    KademliaPeer.removeFromPeers(curr.peer.id);
                }
                curr.peer = new PeerInfo(n, ip);
                KademliaPeer.addPeer(n, ip);
                System.out.printf("Added peer %d with ip %s \n", n, ip);
            }
        }
    }


    /**
     * Remove a peer from the Trie
     *
     * @param s binary representation of a peer id
     */
    public void remove(String s) {
        Node curr = this;
        for (char c : s.toCharArray()) {
            if (curr.isLeaf()) {
                curr.peer = null;
                return;
            } else {
                curr = curr.next.get(c);
            }
        }

        curr.peer = null;
    }


    /**
     * Get the node in a prefix path
     *
     * @param s prefix path
     * @return peer
     */
    public PeerInfo getClosest(String s) {
        Node curr = this;
        for (char c : s.toCharArray()) {
            if (curr.isLeaf()) {
                return curr.peer;
            }

            curr = curr.next.get(c);
        }

        return null;
    }


    /**
     * Method to determine which integer is closer to the peer's id
     *
     * @param a int
     * @param b int
     * @return closer int
     */
    private int closer(int a, int b) {
        return (Config.nodeID ^ a) < (Config.nodeID ^ b) ? a : b;
    }


    /**
     * Prints the routing table
     */
    public void print() {
        TreeMap<String, PeerInfo> table = new TreeMap<>(
                Comparator.comparingInt(String::length)
        );
        printPreOrder(this, new StringBuilder(), table);

        System.out.println("ROUTING TABLE");
        System.out.println(RoutingTable.getNodeString(Config.nodeID) +
                " -> " + Config.nodeID + " (Itself), IP: " + Config.ip);
        for (String s : table.descendingKeySet()) {
            System.out.println(s + " -> " + table.get(s));
        }
        System.out.println();
    }


    /**
     * Helper method to traverse the Trie in pre order
     *
     * @param node  root node
     * @param path  the current path
     * @param table stores path and peer
     */
    private void printPreOrder(Node node, StringBuilder path, TreeMap<String, PeerInfo> table) {
        if (node.isLeaf()) {
            table.put(String.join("", path), node.peer);
        } else {
            path.append('0');
            if (node.next.get('0') != null) printPreOrder(node.next.get('0'), path, table);
            path.deleteCharAt(path.length() - 1);

            path.append('1');
            if (node.next.get('1') != null) printPreOrder(node.next.get('1'), path, table);
            path.deleteCharAt(path.length() - 1);
        }
    }
}