/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

import java.util.Scanner;

/**
 * Implements a client
 */
public class Client {

    /**
     * Method for the Store functionality
     *
     * @param sc Scanner
     */
    public void store(Scanner sc) {
        // take the message from STDIN
        System.out.println("Enter the message to store, and hit enter.");
        sc.nextLine();
        String s = sc.nextLine();

        while (s.length() == 0) {
            System.out.println("Message can't be empty. Enter again: ");
            s = sc.nextLine();
        }

        // get the key of the message from the peer
        int key = PeerClient.storeFromClient(s, Config.bootstrapIP);
        System.out.println("Returned key: " + key);
    }


    /**
     * Method for the Get functionality
     *
     * @param sc Scanner
     */
    public void get(Scanner sc) {
        // Get the key from STDIN
        System.out.println("Enter the key, and hit enter.");
        sc.nextLine();
        String s = sc.nextLine();

        while (s.length() == 0) {
            System.out.println("Key can't be empty. Enter again: ");
            s = sc.nextLine();
        }

        // Receive the message from the peer
        String message = PeerClient.getFromClient(Integer.parseInt(s), Config.bootstrapIP);
        System.out.println("Returned message: " + message);
    }


    /**
     * Shows commands on STDOUT
     */
    public void commands() {
        String commands = "1. store\n" +
                "2. get\n" +
                "3. EXIT";

        try {
            Scanner sc = new Scanner(System.in);
            String commandNum = "";

            while (!"3".equals(commandNum)) {
                System.out.println("\n========  Choose a command =========");
                System.out.println(commands);
                commandNum = sc.next();

                switch (commandNum) {
                    case "1":
                        store(sc);
                        break;
                    case "2":
                        get(sc);
                        break;
                    default:
                        break;
                }
            }

            System.out.println("Exiting program");
            System.exit(-2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
