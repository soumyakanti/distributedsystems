/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static edu.rit.cs.RoutingTable.getNodeString;

/**
 * Implements a data storage where key value pairs are stored.
 * Implemented as a Trie, with HashMap to store key value pairs
 */
public class DataStore {
    public static DataNode root;

    /**
     * Constructor of the data store.
     * Also builds a the prefix tree.
     */
    public DataStore() {
        root = new DataNode();
        build();
        print();
    }

    /**
     * Get all keys and values for the longest matching prefix of key
     *
     * @param key key
     * @return {key: {clientID: value}}
     */
    public static HashMap<Integer, HashMap<Integer, String>> getAll(int key) {
        return root.getAll(RoutingTable.getNodeString(key).toString(), key);
    }

    public static void main(String[] args) {
        DataStore store = new DataStore();
        store.insert(12, 1, "5");
        store.insert(12, 2, "Hello");
        store.insert(6, 2, "Hi");
        store.print();

        System.out.println(store.getAll(12));
    }

    /**
     * Builds the skeleton data table, without inserting any data.
     */
    public void build() {
        StringBuilder nodeString = getNodeString(Config.nodeID);
        root.buildPath(nodeString.toString());
        while (nodeString.length() > 0) {
            int i = nodeString.length() - 1;
            int last = Character.getNumericValue(nodeString.charAt(i));
            nodeString.setCharAt(i, Character.forDigit(last ^ 1, 10));
            root.buildPath(nodeString.toString());
            nodeString.deleteCharAt(i);
        }
    }

    /**
     * Inserts {key: {clientID: value}} into the data store.
     *
     * @param key      hashcode of the message
     * @param clientID id of the client
     * @param value    message to store
     */
    public void insert(int key, int clientID, String value) {
        root.insert(RoutingTable.getNodeString(key).toString(), key, clientID, value);
    }

    /**
     * Prints the prefix tree
     */
    public void print() {
        root.print();
    }

    /**
     * Get the message given a key and client id.
     *
     * @param key      hashcode of the message
     * @param clientID client id
     * @return message/null
     */
    public String get(int key, int clientID) {
        return root.get(RoutingTable.getNodeString(key).toString(), key, clientID);
    }
}


/**
 * Represents the Trie Node for the Data store
 */
class DataNode {
    // {key: {clientID: value}}
    HashMap<Integer, HashMap<Integer, String>> store = null;
    // children
    HashMap<Character, DataNode> next = new HashMap<>();

    /**
     * Builds a particular prefix path
     *
     * @param s prefix
     */
    public void buildPath(String s) {
        DataNode curr = this;
        for (char c : s.toCharArray()) {
            if (!curr.next.containsKey(c)) {
                curr.next.put(c, new DataNode());
            }

            curr = curr.next.get(c);
        }
    }

    /**
     * Inserts {key: {clientID: value}} into the data store.
     *
     * @param keyString binary representation of the key
     * @param key       hashcode of the message
     * @param clientID  id of the client
     * @param value     message to store
     */
    public void insert(String keyString, int key, int clientID, String value) {
        DataNode curr = this;
        // traverse the prefix
        for (char c : keyString.toCharArray()) {
            // if leaf is encountered, store the data
            if (curr.isLeaf()) {
                if (curr.store == null) curr.store = new HashMap<>();
                if (!curr.store.containsKey(key)) {
                    curr.store.put(key, new HashMap<>());
                }
                curr.store.get(key).put(clientID, value);
                System.out.printf("Added {%d: %s} for client %d\n", key, value, clientID);
                KademliaPeer.addKey(key);
                return;
            } else {
                curr = curr.next.get(c);
            }
        }

        // store data in the largest prefix
        if (curr.store == null) curr.store = new HashMap<>();
        if (!curr.store.containsKey(key)) {
            curr.store.put(key, new HashMap<>());
        }
        curr.store.get(key).put(clientID, value);
        KademliaPeer.addKey(key);
        System.out.printf("Added {%d: %s} for client %d\n", key, value, clientID);
    }

    /**
     * Given a key, and a clientID, return the value.
     *
     * @param keyString binary key
     * @param key       key
     * @param clientID  client id
     * @return message
     */
    public String get(String keyString, int key, int clientID) {
        DataNode curr = this;
        for (char c : keyString.toCharArray()) {
            if (curr.isLeaf()) {
                if (curr.store == null) return null;
                if (curr.store.get(key) == null) return null;
                return curr.store.get(key).get(clientID);
            } else {
                curr = curr.next.get(c);
            }
        }

        if (curr != null && curr.store != null && curr.store.get(key) != null) {
            return curr.store.get(key).get(clientID);
        }
        return null;
    }

    /**
     * Get all keys and values for the longest matching prefix of key
     *
     * @param keyString binary key
     * @param key       key
     * @return {key: {clientID: value}}
     */
    public HashMap<Integer, HashMap<Integer, String>> getAll(String keyString, int key) {
        DataNode curr = this;
        for (char c : keyString.toCharArray()) {
            if (curr.isLeaf()) {
                if (curr.store == null) return null;
                HashMap<Integer, HashMap<Integer, String>> result = curr.store;
                curr.store = null;
                System.out.printf("Removing %s from data store\n",
                        result);
                return result;
            } else {
                curr = curr.next.get(c);
            }
        }

        if (curr.isLeaf()) {
            if (curr.store == null) return null;
            HashMap<Integer, HashMap<Integer, String>> result = curr.store;
            curr.store = null;
            System.out.printf("Removing %s from data store and sending to %d\n",
                    result, key);
            return result;
        }

        return null;
    }

    /**
     * Returns true if the node is a leaf
     *
     * @return boolean
     */
    public boolean isLeaf() {
        return next.size() == 0;
    }

    /**
     * prints the prefix tree
     */
    public void print() {
        TreeMap<String, HashMap<Integer, HashMap<Integer, String>>> table = new TreeMap<>(
                (a, b) -> {
                    if (a.length() == b.length()) {
                        if (RoutingTable.getNodeString(Config.nodeID).toString().equals(a)) {
                            return 1;
                        } else return -1;
                    } else {
                        return a.length() - b.length();
                    }
                }
        );
        printPreOrder(this, new StringBuilder(), table);

        System.out.println("DATA STORE");

        while (!table.isEmpty()) {
            Map.Entry<String, HashMap<Integer, HashMap<Integer, String>>> e = table.pollLastEntry();
            System.out.println(e.getKey() + " -> " + e.getValue());
        }

        System.out.println();
    }

    /**
     * Helper method to traverse the trie in pre order
     *
     * @param node  current node
     * @param path  current path
     * @param table resulting Map
     */
    private void printPreOrder(DataNode node, StringBuilder path, TreeMap<String,
            HashMap<Integer, HashMap<Integer, String>>> table) {
        if (node.isLeaf()) {
            table.put(String.join("", path), node.store);
        } else {
            path.append('0');
            if (node.next.get('0') != null) printPreOrder(node.next.get('0'), path, table);
            path.deleteCharAt(path.length() - 1);

            path.append('1');
            if (node.next.get('1') != null) printPreOrder(node.next.get('1'), path, table);
            path.deleteCharAt(path.length() - 1);
        }
    }
}
