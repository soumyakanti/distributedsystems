/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs;

import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Implements the RPC server for peer
 */
public class PeerServer {

    /**
     * Method to start the server
     */
    public void startServer() {
        System.out.println("Server started");
        try (ServerSocket serverSocket = new ServerSocket(Config.port)) {
            while (true) {
                new Handler(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Implements handler class
     */
    public static class Handler extends Thread {
        private Socket socket;
        private Dispatcher dispatcher;

        public Handler(Socket socket) {
            this.socket = socket;
            this.dispatcher = new Dispatcher();
            dispatcher.register(new PeerServerHandler());
        }

        /**
         * Method to read HTTP content and extract the body
         *
         * @param in buffered reader
         * @return JSON string (body)
         * @throws IOException io exception
         */
        private String readRequest(BufferedReader in) throws IOException {
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            return body.toString();
        }

        /**
         * Method to send response
         *
         * @param out  print writer
         * @param resp json response
         */
        private void sendResponse(PrintWriter out, JSONRPC2Response resp) {
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(resp.toJSONString());
            out.flush();
            out.close();
        }

        /**
         * Run method of the Thread class - overridden
         */
        public void run() {
            try {
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));

                // read request
                String body = readRequest(in);

                JSONRPC2Request req = JSONRPC2Request.parse(body);
                JSONRPC2Response resp = dispatcher.process(req, null);

                sendResponse(out, resp);

            } catch (IOException | JSONRPC2ParseException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

