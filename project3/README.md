## Overview

This project implements a pure peer to peer Kademlia DHT and 
 DOES NOT use a centralized "mini-server". There are two kinds of 
 services available - peer and client. I have tried to implement this 
 project by reading the Kademlia paper, however, the algorithms are 
 simpler than what is suggested in the paper.
 
## Run Instructions
*  To run this project, go to the folder DistributedSystems, 
where there project specific Dockerfile, `Dockerfile-project3`,
 and docker-compose file, `docker-compose-project3.yml`
  are available.
*  Run the command `sudo docker-compose -f docker-compose-project3
.yml -p project3 up -d --build --scale peer=5 --scale client=2`
 to start 7 containers, 5 Kademlia peers and 2 clients. 
 Change the `--scale` value as needed.
*  Attach to Kademlia peers using `sudo docker exec -it
 project3_peer_1 bash`. First run `ifconfig` to get the IPv4
  address of the container. Note the last part of the IP address,
  for example, if the IP address is 172.18.0.4, the last part is 4.
  Now run `# ./run.sh 1 4 P` to start a peer with id = 1, and 
  bootstrap ip = 172.18.0.4 (constructed from 4).
* `run.sh` takes 3 arguments, `node id`, `last part of bootstrap ip`,
  and `node type` (P/C - Peer/Client). Boot strap ip is used by peers
  to join the cluster and make initial queries to fill its own 
  routing table, while a client uses the ip to issue STORE and GET
  commands to a peer.
*  Attach to clients using `sudo docker exec -it project3_client_1
 bash`, and run `# ./run.sh 1 4 C` to start subscriber with id = 1
 and boot strap ip = 172.18.0.4 (constructed from 4).
*  Some docker commands are also available in `instructions.txt` 
for convenience.