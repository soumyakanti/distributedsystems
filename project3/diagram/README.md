This folder contains the high level interaction diagram for Kademlia DHT.
The diagram doesn't talk about the low level workings of Kademlia. 
Please open the ``.svg`` file in a browser for best results.