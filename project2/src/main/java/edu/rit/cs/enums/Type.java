/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs.enums;

/**
 * Enum for message types
 */
public enum Type {
    LOGIN,
    LOGOUT,
    PUBLISH,
    ADVERTISE,
    SUBSCRIBE_T,    //subscribe to a topic
    SUBSCRIBE_K,    //subscribe to a keyword
    UNSUBSCRIBE_T,  // unsubscribe to a topic
    UNSUBSCRIBE_K,  // unsubscribe to a keyword
    NOTIFY_T,       // notify a topic
    NOTIFY_T_SUB,
    NOTIFY_K_SUB,
    NOTIFY_E,       // notify an event
    PUB_REL;        // used to release messages at the receiving end
}
