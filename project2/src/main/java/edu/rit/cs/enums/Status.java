/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs.enums;

/**
 * Enum for online or offline status
 */
public enum Status {
    ONLINE, OFFLINE;
}
