/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs.enums;

/**
 * Enum for Quality of Service
 */
public enum QoS {
    AT_MOST, AT_LEAST, EXACTLY;
}
