/**
 * @author: Soumyakanti Das
 *
 */

package edu.rit.cs.comm;

import edu.rit.cs.enums.QoS;
import edu.rit.cs.enums.Type;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * This is a wrapper class which is used to send data
 * over the network. Topics and Events are stored in the
 * content variable.
 */
public class Message implements Serializable {
    // type of the message
    private Type type;
    // id of the sender
    private int senderID;
    // the contents of the message
    private Object content;
    // sender's address
    private InetAddress address;
    // sender's port
    private int port;
    // agent type (E/P/S)
    private String agent;
    // number of message objects generated
    private static long count = 0;
    // unique id of the message
    private String uniqueID;
    // Qos type
    private QoS qos;


    /**
     * Constructor for Message
     *
     * @param type type of message
     * @param senderID id of the sender
     * @param content message contents
     * @param agent sending agent
     */
    public Message(Type type, int senderID, Object content, String agent) {
        this.type = type;
        this.senderID = senderID;
        this.content = content;
        this.agent = agent;
        count++;
        uniqueID = agent + count;
    }

    public Type getType() {
        return type;
    }

    public int getSenderID() {
        return senderID;
    }

    public Object getContent() {
        return content;
    }

    public InetAddress getAddress() {
        return address;
    }

    public void setAddress(InetAddress address) {
        this.address = address;
    }

    public String getAgent() {
        return agent;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public QoS getQos() {
        return qos;
    }

    public void setQos(QoS qos) {
        this.qos = qos;
    }

    @Override
    public String toString() {
        return "Message{" +
                "type=" + type +
                ", id=" + senderID +
                '}';
    }
}
