/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs.comm;

import edu.rit.cs.enums.QoS;
import edu.rit.cs.enums.Type;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Sender implements methods to send data across network using TCP
 */
public class Sender {

    // sleep time before retrying to send again
    private static int sleep = 1000;
    // number of retries
    private static int retries = 10;

    /**
     * This method implements AT_LEAST_ONCE QOS, i.e., it makes application level
     * guarantees that the message will be sent at least once
     *
     * @param message message to be sent
     * @param destination destination address
     * @return boolean
     */
    private static boolean atleastOnceSender(Message message, InetAddress destination) {
        try (
                Socket socket = new Socket(destination, 63000);
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())
        ) {

            out.writeObject(message);
            System.out.println("Sent message");
            return true;

        } catch (IOException e) {
            System.out.println("Couldn't send message");
            return false;
        }
    }


    /**
     * Helper method to send messages exactly once.
     *
     * @param message message to be sent
     * @param destination destination address
     * @return boolean
     */
    private static boolean QOS2Send(Message message, InetAddress destination) {
        try (
                Socket socket = new Socket(destination, 64000);
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())
        ) {
            out.writeObject(message);
            return true;
        } catch (IOException e) {
            return false;
        }
    }


    /**
     * This method makes application level guarantee that the message will be
     * sent exactly once.
     *
     * @param message message to be sent
     * @param destination destination address
     */
    private static void exactlyOnceSender(Message message, InetAddress destination) {
        // get the unique ID of the message
        String messageID = message.getUniqueID();

        // send the unique ID in a separate message
        Message info = new Message(Type.PUB_REL, message.getSenderID(),
                messageID, message.getAgent());
        message.setQos(QoS.EXACTLY);

        boolean firstMessageSent = false;

        // Try sending the message with retries.
        for (int i = 1; i <= retries; i++) {
            firstMessageSent = QOS2Send(message, destination);
            if (firstMessageSent) break;

            System.out.println("retrying...");
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Try sending the Info message if the first message is sent successfully
        if (firstMessageSent) {
            for (int i = 1; i <= retries; i++) {
                if (QOS2Send(info, destination)) break;
                System.out.println("retrying...");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * This method is called by all agents to send data over the network.
     *
     * @param message message to be sent
     * @param destination destination address
     * @param qos QOS
     */
    public static void sendTCP(Message message, InetAddress destination, QoS qos) {
        if (qos == QoS.EXACTLY) {
            exactlyOnceSender(message, destination);
        } else {
            sendAtMostOrAtleastOnce(message, destination, qos);
        }
    }


    /**
     * This message implements AT_LEAST_ONCE and AT_MOST_ONCE QoS.
     *
     * @param message message to be sent
     * @param destination destination address
     * @param qos QOS
     */
    private static void sendAtMostOrAtleastOnce(Message message, InetAddress destination, QoS qos) {
        try (
                Socket socket = new Socket(destination, 63000);
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())
                ) {

            // try sending the message
            out.writeObject(message);

        } catch (IOException e) {

            // if QoS.AT_MOST, we don't need to do anything

            // If QOS.AT_LEAST, retry sending the message
            if (qos == QoS.AT_LEAST) {
                new Thread(() -> {
                    for (int i = 1; i <= retries; i++) {
                        System.out.print("Retry " + i + ": ");
                        boolean delivered = atleastOnceSender(message, destination);
                        if (delivered) break;
                        try {
                            Thread.sleep(sleep);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }
                }).start();
            }

        }
    }
}
