/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs.comm;

import edu.rit.cs.agent.Agent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Receiver is a thread that listens on a port and
 * inserts the received messages in a queue.
 */
public class Receiver extends Thread{
    // agent associated with the receiver
    private Agent agent;
    // receiving port
    private int port;

    public Receiver(Agent agent, int port) {
        this.agent = agent;
        this.port = port;
    }


    /**
     * Keep receiving TCP messages
     */
    private void receiveTCP() {
        Message message;
        try(
            ServerSocket listenSocket = new ServerSocket(port)
            )
        {
            while (true) {
                Socket clientSocket = listenSocket.accept();
                ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());

                // read the message object
                message = (Message) in.readObject();

                // set address and port
                message.setAddress(clientSocket.getInetAddress());
                message.setPort(clientSocket.getPort());

                // insert the message into the queue of the agent
                agent.insertIntoMessageQueue(message);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * Run method to start the thread
     */
    @Override
    public void run() {
        receiveTCP();
    }
}
