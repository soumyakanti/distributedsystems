/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs.comm;

import edu.rit.cs.agent.Agent;
import edu.rit.cs.enums.Type;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;


/**
 * This class implements a QOS2 receiver, and listens on a separate port.
 *
 */
public class QoS2Receiver extends Thread{
    // agent it is associated with
    private Agent agent;
    // listening port
    private int port;
    Hashtable<String, Message> messages;

    public QoS2Receiver(Agent agent, int port) {
        this.agent = agent;
        this.port = port;
        messages = new Hashtable<>();
    }

    /**
     * Receives Messages with QOS.EXACTLY.
     */
    private void receiveTCP() {
        Message message;
        try(
                ServerSocket listenSocket = new ServerSocket(port)
            )
        {
            while (true) {
                Socket clientSocket = listenSocket.accept();
                ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());

                // get the message
                message = (Message)in.readObject();

                // if the type is not PUB_REL, it is a normal message
                // and is put in the messages hashtable temporarily
                if (message.getType() != Type.PUB_REL) {
                    messages.put(message.getUniqueID(), message);

                // if the message type is PUB_REL, then the corresponding
                // message is taken out of the hashtable and put into the
                // message queue of the agent
                } else {
                    String messageID = (String) message.getContent();
                    if (messages.containsKey(messageID)) {
                        agent.insertIntoMessageQueue(messages.remove(messageID));
                    }
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calls receiveTCP when the thread is started
     */
    public void run() {
        receiveTCP();
    }
}
