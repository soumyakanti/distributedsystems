/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs.processor;

import edu.rit.cs.agent.Agent;

/**
 * Processor thread is used to start processing
 * messages for an agent
 */
public class Processor extends Thread {
    // agent associated with the Processor
    private Agent agent;

    public Processor(Agent agent) {
        this.agent = agent;
    }

    public void run() {
        while (true) {
            agent.process();
        }
    }
}
