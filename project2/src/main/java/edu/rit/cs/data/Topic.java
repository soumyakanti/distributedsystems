/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs.data;

import java.io.Serializable;
import java.util.List;

/**
 * Topic class implements a topic, which has ID, keywords and a name
 */
public class Topic implements Serializable {
	private int id;
	private List<String> keywords;
	private String name;

	public Topic(int id, List<String> keywords, String name) {
		this.id = id;
		this.keywords = keywords;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return String.format("Topic[id: %d, name: %s, keywords: %s]", id, name, keywords);
	}
}
