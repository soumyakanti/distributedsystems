/**
 * @author: Soumyakanti Das
 *
 */

package edu.rit.cs.data;

import java.io.Serializable;


/**
 * Implements an event, containing topic, title and content
 */
public class Event implements Serializable {
	private int id;
	private Topic topic;
	private String title;
	private String content;

	public Event(int id, Topic topic, String title, String content) {
		this.id = id;
		this.topic = topic;
		this.title = title;
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Topic getTopic() {
		return topic;
	}

	public String toString() {
		return String.format("Event[id: %d, title: %s, Topic: %s, content: %s]",
				id, title, topic, content);
	}
}
