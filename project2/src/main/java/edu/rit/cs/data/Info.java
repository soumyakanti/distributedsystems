/**
 * @author: Soumyakanti Das
 *
 */

package edu.rit.cs.data;

import edu.rit.cs.enums.Status;

import java.net.InetAddress;

/**
 * Info is used to store connection info of an agent
 */
public class Info {
    // address from where the agent connected last
    private InetAddress address;
    // port used
    private int port;
    // ONLINE or OFFLINE
    private Status status;

    public Info(InetAddress address, int port, Status status) {
        this.address = address;
        this.status = status;
        this.port = port;
    }

    public InetAddress getAddress() {
        return address;
    }

    public Status getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Info{" +
                "address=" + address +
                ", port=" + port +
                ", status=" + status +
                '}';
    }
}
