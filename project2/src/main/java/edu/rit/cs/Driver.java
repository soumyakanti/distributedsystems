/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs;

import edu.rit.cs.agent.Agent;
import edu.rit.cs.agent.EventManager;
import edu.rit.cs.agent.Publisher;
import edu.rit.cs.agent.Subscriber;
import edu.rit.cs.comm.QoS2Receiver;
import edu.rit.cs.comm.Receiver;
import edu.rit.cs.processor.Processor;

/**
 * Driver runs the main method and starts the agent
 */
public class Driver {

    public static void main(String[] args) {
        // args can be of 0 or 3 length
        // 1 in case of event manager
        // in case of pub/sub, need ip of manager, id, and P/S

        Agent agent = null;

        // condition for the agent to be an event manager
        if (args.length == 1) {
            agent = new EventManager();
        } else if (args.length == 3) {
            String ip = args[0];
            int id = Integer.parseInt(args[1]);
            String identifier = args[2];

            // publisher
            if ("P".equals(identifier)) {
                agent = new Publisher(id, ip);

            // subscriber
            } else {
                agent = new Subscriber(id, ip);
            }

        } else {
            System.out.println("Usage: java Driver [ip_of_event_manager id P/S]");
            System.exit(-1);
        }

        // start the receiver
        Receiver receiver = new Receiver(agent, 63000);
        QoS2Receiver qoS2Receiver = new QoS2Receiver(agent, 64000);
        receiver.start();
        qoS2Receiver.start();

        // start the processor
        Processor processor = new Processor(agent);
        processor.start();

        // Run the commands method
        if (agent instanceof Publisher) {
            ((Publisher) agent).commands();
        } else if (agent instanceof Subscriber) {
            ((Subscriber) agent).commands();
        }
    }
}
