/**
 * @author: Soumyakanti Das
 *
 */

package edu.rit.cs.agent;

import edu.rit.cs.comm.Message;
import edu.rit.cs.comm.Sender;
import edu.rit.cs.data.Event;
import edu.rit.cs.data.Info;
import edu.rit.cs.data.Topic;
import edu.rit.cs.enums.QoS;
import edu.rit.cs.enums.Status;
import edu.rit.cs.enums.Type;

import java.util.*;

/**
 * Implements the event manager, which acts as a broker
 * between publishers and subscribers.
 */
public class EventManager implements Agent{

    // Stores login details for the publishers
    // Maps pub id to its Info.
    private Hashtable<Integer, Info> publishers;
    // Stores login details for the subscribers
    // Maps sub id to its Info.
    private Hashtable<Integer, Info> subscribers;
    // Maps topic name to a set of subscriber IDs.
    private Hashtable<String, Set<Integer>> subscribedTopics;
    // Maps keywords to a set of subscriber IDs.
    private Hashtable<String, Set<Integer>> subscribedKeywords;
    // Maps pub ID to a map of topic name to Topics.
    // It is used to store publisher ID and their Topics.
    private Hashtable<Integer, Hashtable<String, Topic>> topics;
    // Set of all available topics.
    private HashSet<Topic> allTopics;
    // Maps Subscriber ID to their events which are pending notification.
    private Hashtable<Integer, Queue<Event>> events;
    // Queue of received messages, waiting to be processed.
    private Queue<Message> messageQueue;
    // lock for the message queue
    private final Object lock = new Object();

    /**
     * Default constructor, initialise data structures.
     */
    public EventManager() {
        this.messageQueue = new LinkedList<>();
        publishers = new Hashtable<>();
        subscribers = new Hashtable<>();
        subscribedTopics = new Hashtable<>();
        subscribedKeywords = new Hashtable<>();
        topics = new Hashtable<>();
        allTopics = new HashSet<>();
        events = new Hashtable<>();
        System.out.println("Event manager started...");
    }

    /**
     * Inserts messages into the message Queue.
     *
     * @param message received message
     */
    public void insertIntoMessageQueue(Message message) {
        synchronized (lock) {
            this.messageQueue.offer(message);
            // notify the waiting processor thread.
            lock.notify();
        }
    }

    /**
     * Handles login messages from publishers and subscribers.
     *
     * @param message the login message
     */
    private void handleLogin(Message message) {
        int id = message.getSenderID();
        String agentType = message.getAgent();

        // if message received from pub
        if ("P".equals(agentType)) {
            System.out.println("Logging in Publisher " + id);
            publishers.put(id, new Info(message.getAddress(), message.getPort(), Status.ONLINE));
        // if message received from sub
        } else {
            System.out.println("Logging in Subscriber " + id);
            subscribers.put(id, new Info(message.getAddress(), message.getPort(), Status.ONLINE));
        }

        // if agent is subscriber, tell them what topics they are subscribed to and all other topics
        if ("S".equals(agentType)) {

            Set<Topic> topicsToSend = new HashSet<>();
            Set<String> keywordsToSend = new HashSet<>();
            for (Topic t: allTopics) {
                String name = t.getName();
                List<String> keywords = t.getKeywords();
                if (subscribedTopics.get(name) != null && subscribedTopics.get(name).contains(id)) {
                    topicsToSend.add(t);
                }

                for (String key: keywords) {
                    if (subscribedKeywords.containsKey(key) && subscribedKeywords.get(key).contains(id)) {
                        keywordsToSend.add(key);
                    }
                }
            }

            for (Topic t: topicsToSend) {
                Message m = new Message(Type.NOTIFY_T_SUB, 0, t, "E");
                Sender.sendTCP(m, message.getAddress(), QoS.AT_LEAST);
            }
            for (String k: keywordsToSend) {
                Message m = new Message(Type.NOTIFY_K_SUB, 0, k, "E");
                Sender.sendTCP(m, message.getAddress(), QoS.AT_LEAST);
            }

            for (Topic t: allTopics) {
                Message m = new Message(Type.NOTIFY_T, 0, t, "E");
                Sender.sendTCP(m, message.getAddress(), QoS.AT_LEAST);
            }
            // else if the agent is a publisher, tell them their topics and other topics
        } else {
            if (topics.containsKey(id)) {
                for (String topicName : topics.get(id).keySet()) {
                    Topic t = topics.get(id).get(topicName);
                    Message m = new Message(Type.NOTIFY_T_SUB, 0, t, "E");
                    Sender.sendTCP(m, message.getAddress(), QoS.EXACTLY);
                }

                for (Topic t : allTopics) {
                    if (!topics.get(id).containsKey(t.getName())) {
                        Message m = new Message(Type.NOTIFY_T, 0, t, "E");
                        Sender.sendTCP(m, message.getAddress(), QoS.AT_LEAST);
                    }
                }
            } else {
                for (Topic t: allTopics) {
                    Message m = new Message(Type.NOTIFY_T, 0, t, "E");
                    Sender.sendTCP(m, message.getAddress(), QoS.AT_LEAST);
                }
            }
        }

        // if subscriber logs in, and has pending events, notify all pending events.
        if ("S".equals(agentType)) {
            if (events.containsKey(id)) {
                Queue<Event> pending = events.get(id);
                System.out.println("Pending events: " + pending);
                int size = pending.size();

                for (int i = 0; i < size; i++) {
                    Event event = pending.poll();
                    Message m = new Message(Type.NOTIFY_E, 0, event, "E");
                    System.out.println("Notifying Subscriber " + id + " event" + event);
                    Sender.sendTCP(m, message.getAddress(), QoS.EXACTLY);
                }
            }
        }
    }

    /**
     * Handles logout messages from publishers and subscribers.
     *
     * @param message the logout message.
     */
    private void handleLogout(Message message) {
        int id = message.getSenderID();
        String agentType = message.getAgent();
        if ("P".equals(agentType)) {
            System.out.println("Logging out Publisher " + id);
            publishers.put(id, new Info(message.getAddress(), message.getPort(), Status.OFFLINE));
        } else {
            System.out.println("Logging out Subscriber " + id);
            subscribers.put(id, new Info(message.getAddress(), message.getPort(), Status.OFFLINE));
        }
    }


    /**
     * Handle message advertises from publishers.
     *
     * @param message the advertise message
     */
    private void handleAdvertise(Message message) {
        int id = message.getSenderID();
        Topic topic = (Topic)message.getContent();

        // add this topic in the set of all topics
        allTopics.add(topic);

        System.out.println("Topic advertisement from Publisher " + id + ", " + topic);

        // add this topic to the topics hashtable with this publisher's ID.
        if (!topics.containsKey(id)) {
            topics.put(id, new Hashtable<>());
        }

        Hashtable<String, Topic> pubTopics = topics.get(id);
        pubTopics.put(topic.getName(), topic);

        // notify all other publishers of this new topic
        for (int pubId: publishers.keySet()) {
            if (pubId == id) continue;
            Info info = publishers.get(pubId);
            if (info.getStatus() == Status.ONLINE) {
                Message m = new Message(Type.NOTIFY_T, 0, topic, "E");
                Sender.sendTCP(m, info.getAddress(), QoS.AT_LEAST);
            }
        }

        // notify all subscribers of this new topic
        for (int subId: subscribers.keySet()) {
            Info info = subscribers.get(subId);
            if (info.getStatus() == Status.ONLINE) {
                Message m = new Message(Type.NOTIFY_T, 0, topic, "E");
                Sender.sendTCP(m, info.getAddress(), QoS.AT_LEAST);
            }
        }
    }


    /**
     * Handles publish messages from publishers.
     *
     * @param message the publish message
     */
    private void handlePublish(Message message) {
        Event event = (Event)message.getContent();

        // extract the topic and keywords from the message
        Topic topic = event.getTopic();
        List<String> keywords = topic.getKeywords();

        // create a set of subscriber IDs to be notified of new event.
        Set<Integer> subs = new HashSet<>();

        // add all subscribers who subscribed to this topic
        if (subscribedTopics.containsKey(topic.getName())) {
            subs.addAll(subscribedTopics.get(topic.getName()));
        }

        // add all subscribers who subscribed to any of the keyword
        for (String kw: keywords) {
            if (subscribedKeywords.containsKey(kw)) {
                subs.addAll(subscribedKeywords.get(kw));
            }
        }

        // create a new message with the received event
        Message m = new Message(Type.NOTIFY_E, 0, event, "E");

        // For all the affected subscribers, if they are online, notify them,
        // or store the events in the events hashtable.
        for (int subID: subs) {
            Info info = subscribers.get(subID);
            if (info.getStatus() ==  Status.OFFLINE) {
                if (!events.containsKey(subID)) {
                    events.put(subID, new LinkedList<>());
                }

                events.get(subID).offer(event);
                System.out.printf("Event %s stored for Subscriber %d\n", event, subID);
            } else {
                Sender.sendTCP(m, info.getAddress(), QoS.EXACTLY);
                System.out.printf("Event %s notified to Subscriber %d\n", event, subID);
            }
        }
    }

    /**
     * Handles subscribe requests for topics from subscribers.
     *
     * @param message received message
     */
    private void handleSubscribeTopic(Message message) {
        int id = message.getSenderID();
        Topic topic = (Topic) message.getContent();
        String name = topic.getName();

        if (!subscribedTopics.containsKey(name)) {
            subscribedTopics.put(name, new HashSet<>());
        }

        // add this subscriber's ID as a value to the topic name key
        subscribedTopics.get(name).add(id);
        System.out.printf("Subscriber %d subscribed to topic %s \n", id, name);
    }


    /**
     * Handles subscribe requests for keywords from subscribers.
     *
     * @param message received message
     */
    private void handleSubscribeKeyword(Message message) {
        int id = message.getSenderID();
        String name = (String) message.getContent();

        if (!subscribedKeywords.containsKey(name)) {
            subscribedKeywords.put(name, new HashSet<>());
        }

        subscribedKeywords.get(name).add(id);
        System.out.printf("Subscriber %d subscribed to keyword %s \n", id, name);
    }


    /**
     * Handles unsubscribe requests for topics from subscribers.
     *
     * @param message received message
     */
    private void handleUnsubscribeTopic(Message message) {
        int id = message.getSenderID();
        Topic topic = (Topic) message.getContent();
        String name = topic.getName();

        if (subscribedTopics.containsKey(name)) {
            if (subscribedTopics.get(name).contains(id)) {
                subscribedTopics.get(name).remove(id);
                System.out.printf("Subscriber %d unsubscribed to topic %s \n", id, name);
            }
        }
    }


    /**
     * Handles unsubscribe requests for keywords from subscribers.
     *
     * @param message received message
     */
    private void handleUnsubscribeKeyword(Message message) {
        int id = message.getSenderID();
        String name = (String) message.getContent();

        if (subscribedKeywords.containsKey(name)) {
            if (subscribedKeywords.get(name).contains(id)) {
                subscribedKeywords.get(name).remove(id);
                System.out.printf("Subscriber %d unsubscribed to keyword %s \n", id, name);
            }
        }
    }

    /**
     * Process messages from the message queue
     */
    public void process() {
        synchronized (lock) {
            while (!messageQueue.isEmpty()) {
                // get a message from the message queue.
                Message message = messageQueue.poll();
                // get the type of the message.
                Type type = message.getType();

                // process based on the type
                switch (type) {
                    case LOGIN:
                        new Thread(() -> handleLogin(message)).start();
                        break;
                    case LOGOUT:
                        handleLogout(message);
                        break;
                    case ADVERTISE:
                        new Thread(() -> handleAdvertise(message)).start();
                        break;
                    case PUBLISH:
                        new Thread(() -> handlePublish(message)).start();
                        break;
                    case SUBSCRIBE_T:
                        handleSubscribeTopic(message);
                        break;
                    case SUBSCRIBE_K:
                        handleSubscribeKeyword(message);
                        break;
                    case UNSUBSCRIBE_T:
                        handleUnsubscribeTopic(message);
                        break;
                    case UNSUBSCRIBE_K:
                        handleUnsubscribeKeyword(message);
                        break;
                    default:
                        break;
                }
            }

            // wait to be notified by the receiver if a new message arrives.
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
