/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs.agent;

import edu.rit.cs.comm.Message;

/**
 * Interface Agent is implemented by Publisher, Subscriber and
 * EventManager
 */
public interface Agent {

    /**
     * Inserts messages into the message Queue.
     *
     * @param message received message
     */
    void insertIntoMessageQueue(Message message);

    /**
     * Process messages from the message queue
     */
    void process();
}
