/**
 * @author: Soumyakanti Das
 *
 */

package edu.rit.cs.agent;

import edu.rit.cs.comm.Message;
import edu.rit.cs.comm.Sender;
import edu.rit.cs.enums.QoS;
import edu.rit.cs.enums.Status;
import edu.rit.cs.enums.Type;
import edu.rit.cs.data.Event;
import edu.rit.cs.data.Topic;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Implements a publisher, which can advertise and
 * publish events to the event manager.
 */
public class Publisher implements Agent{

    private Status status;
    // the publisher ID, taken from the user
    private int id;
    // topic and event IDs are generated automatically
    private int topicID = 0;
    private int eventID = 0;
    // Maps topic name to the Topic
    private Hashtable<String, Topic> topics;
    // Stores topic names of the topics advertised by other publishers.
    private HashSet<String> otherTopics;
    // address of the event manager
    private InetAddress eventManagerAddress;
    // Queue of messages to store the received messages
    private Queue<Message> messageQueue;
    // lock for the message queue.
    private final Object lock = new Object();

    /**
     * Constructor for the publisher to initialise all variables and
     * data structures.
     *
     * @param id id of the publisher
     * @param ip ip of the event manager
     */
    public Publisher(int id, String ip) {
        this.id = id;
        try {
            this.eventManagerAddress = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        topics = new Hashtable<>();
        otherTopics = new HashSet<>();
        messageQueue = new LinkedList<>();
        status = Status.OFFLINE;
    }


    /**
     * Publishes an event to the event manager.
     *
     * @param event event to be published
     */
    private void publish(Event event) {
        System.out.println("Publishing " + event);
        Message message = new Message(Type.PUBLISH, id, event, "P");
        Sender.sendTCP(message, eventManagerAddress, QoS.EXACTLY);
    }


    /**
     * Advertises a topic to the event manager.
     *
     * @param newTopic topic to be advertised
     */
    private void advertise(Topic newTopic) {
        System.out.println("Advertising " + newTopic);
        Message message = new Message(Type.ADVERTISE, id, newTopic, "P");
        Sender.sendTCP(message, eventManagerAddress, QoS.EXACTLY);
    }


    /**
     * Sends a log in message to the event manager.
     *
     */
    private void logIn() {
        System.out.println("logging in");
        Message message = new Message(Type.LOGIN, id, null, "P");
        Sender.sendTCP(message, eventManagerAddress, QoS.AT_LEAST);
        status = Status.ONLINE;
    }


    /**
     * Sends a log out message to the event manager.
     */
    private void logOut() {
        System.out.println("logging out");
        Message message = new Message(Type.LOGOUT, id, null, "P");
        Sender.sendTCP(message, eventManagerAddress, QoS.AT_MOST);
        status = Status.OFFLINE;
    }


    /**
     * Lists all topics to the console.
     */
    private void listTopics() {
        System.out.println("This publisher's topics:");
        System.out.println(topics.keySet());

        System.out.println("Other topics:");
        System.out.println(otherTopics);
    }


    /**
     * Helper method to create a topic from user inputs through
     * the console and call advertise method.
     *
     * @param sc Scanner
     */
    private void advertiseHelper(Scanner sc) {
        System.out.println("Enter name of the topic to be published: ");
        String name = sc.next();

        while (name.length() == 0 || topics.containsKey(name) || otherTopics.contains(name)) {
            System.out.println("Topic already present, select another name: ");
            name = sc.next();
        }

        System.out.println("Enter keywords separated by commas(,): ");
        sc.nextLine();
        String keywords = sc.nextLine();

        while (keywords.length() == 0) {
            System.out.println("Keywords can't be empty. Enter keywords separated by commas(,): ");
            keywords = sc.nextLine();
        }

        String [] keywordsArray = keywords.replace(" ", "").split(",");
        Topic topic = new Topic(topicID++, Arrays.asList(keywordsArray), name);
        topics.put(name, topic);
        advertise(topic);
    }


    /**
     * Helper method to create an Event from user inputs through the console,
     * and call the publish method.
     *
     * @param sc Scanner
     */
    private void publishHelper(Scanner sc) {
        System.out.println("Enter title of the event to be published: ");
        String title = sc.nextLine();
        while (title.length() == 0) {
            title = sc.nextLine();
        }

        System.out.println("Enter contents of the event to be published: ");
        String content = sc.nextLine();
        while (content.length() == 0) {
            content = sc.nextLine();
        }

        System.out.println("Enter topic name of the event: ");
        String topicName = sc.nextLine();
        while (topicName.length() == 0) {
            topicName = sc.nextLine();
        }

        while (!topics.containsKey(topicName)) {
            System.out.println("This publisher's topics:");
            System.out.println(topics.keySet());
            System.out.println("No such topic. Enter topic name of the event: ");
            topicName = sc.nextLine();
        }

        Event event = new Event(eventID++, topics.get(topicName), title, content);
        publish(event);
    }

    /**
     * Takes inputs from user through the console and provides functionality
     * related to publisher.
     */
    public void commands() {
        String commands = "1. Log in\n" +
                          "2. Log out\n" +
                          "3. List topics\n" +
                          "4. Advertise Topic\n" +
                          "5. Publish Event\n" +
                          "6. EXIT\n";

        try {
            Scanner sc = new Scanner(System.in);
            String commandNum = "";

            while (!"6".equals(commandNum)) {
                System.out.println("\n========  Choose a command =========");
                System.out.println(commands);
                commandNum = sc.next();

                switch (commandNum) {
                    case "1":
                        logIn();
                        break;
                    case "2":
                        if (status == Status.ONLINE)
                            logOut();
                        else System.out.println("Not logged in");
                        break;
                    case "3":
                        listTopics();
                        break;
                    case "4":
                        if (status == Status.ONLINE)
                            advertiseHelper(sc);
                        else System.out.println("Not logged in");
                        break;
                    case "5":
                        if (status == Status.ONLINE)
                            publishHelper(sc);
                        else System.out.println("Not logged in");
                        break;
                    default:
                        break;
                }
            }

            System.out.println("Exiting program");
            System.exit(-2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Inserts messages into the message Queue.
     *
     * @param message received message
     */
    @Override
    public void insertIntoMessageQueue(Message message) {
        synchronized (lock) {
            this.messageQueue.offer(message);
            lock.notify();
        }
    }


    /**
     * Process messages from the message queue
     */
    public void process() {
        synchronized (lock) {
            while (!messageQueue.isEmpty()) {
                Message message = messageQueue.poll();

                if (message.getType() == Type.NOTIFY_T) {
                    String name = ((Topic)message.getContent()).getName();
                    otherTopics.add(name);
                    System.out.println("Topic notification: " + name);
                } else if (message.getType() == Type.NOTIFY_T_SUB) {
                    String name = ((Topic)message.getContent()).getName();
                    topics.put(name, (Topic)message.getContent());
                }
            }

            // wait for the receiver thread to notify
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
