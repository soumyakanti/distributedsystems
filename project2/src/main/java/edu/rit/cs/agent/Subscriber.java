/**
 * @author: Soumyakanti Das
 *
 */
package edu.rit.cs.agent;

import edu.rit.cs.comm.Message;
import edu.rit.cs.comm.Sender;
import edu.rit.cs.data.Event;
import edu.rit.cs.data.Topic;
import edu.rit.cs.enums.QoS;
import edu.rit.cs.enums.Status;
import edu.rit.cs.enums.Type;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Implements a subscriber, which can subscribe to topics and
 * keywords, and can get notified by the event manager.
 */
public class Subscriber implements Agent{

    private Status status;
    // subscriber id, taken from the user
    private int id;
    // Stores all topics to which this subscriber is
    // subscribed to
    private Hashtable<String, Topic> subscribedTopics;
    // Stores all keywords to which this subscriber is
    // subscribed to
    private Set<String> subscribedKeywords;
    // Stores all available topics
    private Hashtable<String, Topic> otherTopics;
    // stores all available keywords
    private Set<String> otherKeywords;
    // address of the event manager
    private InetAddress address;
    // message queue to store received messages
    private Queue<Message> messageQueue;
    // lock for the message queue
    private final Object lock = new Object();


    /**
     * Constructor of the Subscriber to initialize all variables
     * and data structures.
     *
     * @param id id of the subscriber, taken from user
     * @param ip ip of the event manager
     */
    public Subscriber(int id, String ip) {
        this.id = id;
        try {
            this.address = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        subscribedTopics = new Hashtable<>();
        subscribedKeywords = new HashSet<>();
        otherTopics = new Hashtable<>();
        otherKeywords = new HashSet<>();
        messageQueue = new LinkedList<>();
        status = Status.OFFLINE;
    }


    /**
     * Sends a login message to the event manager
     */
    private void login() {
        System.out.println("logging in");
        Message message = new Message(Type.LOGIN, id, null, "S");
        Sender.sendTCP(message, address, QoS.AT_LEAST);
        status = Status.ONLINE;
    }


    /**
     * Sends a logout message to the event manager
     */
    private void logout() {
        System.out.println("log out");
        Message message = new Message(Type.LOGOUT, id, null, "S");
        Sender.sendTCP(message, address, QoS.AT_MOST);
        status = Status.OFFLINE;
    }


    /**
     * Subscribes to a topic, by sending a message to the event manager.
     *
     * @param topic topic to subscribe
     */
    private void subscribe(Topic topic) {
        System.out.println("Subscribing to " + topic);
        Message message = new Message(Type.SUBSCRIBE_T, id, topic, "S");
        Sender.sendTCP(message, address, QoS.AT_LEAST);
    }


    /**
     * Helper method to take input from the user and then subscribe to the topic.
     *
     * @param sc Scanner
     */
    private void subscribeHelperTopic(Scanner sc) {
        System.out.println("Enter name of the topic to subscribe to: ");

        String name = sc.nextLine();
        while (name.length() == 0) {
            name = sc.nextLine();
        }

        if (subscribedTopics.containsKey(name)) {
            System.out.println("Already subscribed to " + name);
            return;
        }

        if (!otherTopics.containsKey(name)) {
            System.out.println("No such topic exists");
            System.out.println("Available topics: " + otherTopics.keySet());
            return;
        }

        subscribedTopics.put(name, otherTopics.get(name));

        subscribe(otherTopics.get(name));
    }


    /**
     * Subscribe to a keyword by sending a message to the event manager
     *
     * @param keyword keyword
     */
    private void subscribe(String keyword) {
        System.out.println("Subscribing to " + keyword);
        Message message = new Message(Type.SUBSCRIBE_K, id, keyword, "S");
        Sender.sendTCP(message, address, QoS.AT_LEAST);
    }


    /**
     * Helper method to take input from the user and then subscribe.
     *
     * @param sc Scanner
     */
    private void subscribeHelperKeyword(Scanner sc) {
        System.out.println("Enter the keyword to subscribe to: ");

        String name = sc.nextLine();
        while (name.length() == 0) {
            name = sc.nextLine();
        }

        if (subscribedKeywords.contains(name)) {
            System.out.println("Already subscribed to " + name);
            return;
        }

        if (!otherKeywords.contains(name)) {
            System.out.println("No such keyword exists");
            System.out.println("Available keywords: " + otherKeywords);
            return;
        }

        subscribedKeywords.add(name);

        subscribe(name);
    }


    /**
     * Unsubscribe to a topic by sending a message to the event manager.
     *
     * @param topic topic to unsubscribe
     */
    private void unsubscribe(Topic topic) {
        System.out.println("Unsubscribed to " + topic);
        Message message = new Message(Type.UNSUBSCRIBE_T, id, topic, "S");
        Sender.sendTCP(message, address, QoS.AT_LEAST);
        subscribedTopics.remove(topic.getName());
    }


    /**
     * Helper method to unsubscribe to a topic
     *
     * @param sc Scanner
     */
    private void unsubscribeHelperTopic(Scanner sc) {
        System.out.println("Enter name of the topic to unsubscribe: ");

        String name = sc.nextLine();
        while (name.length() == 0) {
            name = sc.nextLine();
        }

        if (!subscribedTopics.containsKey(name)) {
            System.out.println("Not subscribed to " + name);
            return;
        }

        if (!otherTopics.containsKey(name)) {
            System.out.println("No such topic exists");
            System.out.println("Available topics: " + otherTopics.keySet());
            return;
        }

        subscribedTopics.remove(name);

        unsubscribe(otherTopics.get(name));
    }


    /**
     * Unsubscribe to a keyword by sending a message to the event manager
     *
     * @param keyword keyword
     */
    private void unsubscribe(String keyword) {
        System.out.println("Unsubscribed to " + keyword);
        Message message = new Message(Type.UNSUBSCRIBE_K, id, keyword, "S");
        Sender.sendTCP(message, address, QoS.AT_LEAST);
        subscribedKeywords.remove(keyword);
    }


    /**
     * Unsubscribe from all topics and keywords
     */
    private void unsubscribeAll() {
        // unsubscribe to all subscribed topics
        Set<String> topicNames = subscribedTopics.keySet();
        for (String name: topicNames) {
            unsubscribe(subscribedTopics.get(name));
        }

        // unsubscribe to all subscribed keywords
        Set<String> keyNames = new HashSet<>(subscribedKeywords);
        for (String keyword: keyNames) {
            unsubscribe(keyword);
        }
    }


    /**
     * Helper method to unsubscribe keyword
     *
     * @param sc Scanner
     */
    private void unsubscribeHelperKeyword(Scanner sc) {
        System.out.println("Enter the keyword to unsubscribe: ");

        String name = sc.nextLine();
        while (name.length() == 0) {
            name = sc.nextLine();
        }

        if (!subscribedKeywords.contains(name)) {
            System.out.println("Not subscribed to " + name);
            return;
        }

        if (!otherKeywords.contains(name)) {
            System.out.println("No such keyword exists");
            System.out.println("Available keywords: " + otherKeywords);
            return;
        }

        subscribedKeywords.remove(name);

        unsubscribe(name);
    }


    /**
     * List all subscribed topics and keywords.
     */
    private void listSubscribed() {
        System.out.println("Subscribed topics: ");
        System.out.println(subscribedTopics);

        System.out.println("Subscribed keywords: ");
        System.out.println(subscribedKeywords);
    }


    /**
     * Handles topic notification sent by the event manager
     *
     * @param message Received message
     */
    private void handleNotificationTopic(Message message) {
        Topic topic = (Topic) message.getContent();
        if (!subscribedTopics.containsKey(topic.getName())) {
            System.out.println("New topic notification: " + topic);
        }
        otherTopics.put(topic.getName(), topic);
        otherKeywords.addAll(topic.getKeywords());
    }


    /**
     * Handles topic subscribed notification sent by the event manager
     *
     * @param message Received message
     */
    private void handleNotificationTopicSub(Message message) {
        Topic topic = (Topic) message.getContent();
        subscribedTopics.put(topic.getName(), topic);
        otherTopics.put(topic.getName(), topic);
        otherKeywords.addAll(topic.getKeywords());
    }

    /**
     * Handles keyword subscribed notification sent by the event manager
     *
     * @param message Received message
     */
    private void handleNotificationKeywordSub(Message message) {
        String key = (String) message.getContent();
        subscribedKeywords.add(key);
        otherKeywords.add(key);
    }


    /**
     * Handles event notification from the event manager
     *
     * @param message received message
     */
    private void handleNotificationEvent(Message message) {
        Event event = (Event) message.getContent();

        System.out.println("New event notification: " + event);
    }


    /**
     * Takes input from the user to perform different functions.
     */
    public void commands() {
        String commands =
                "1. Log in\n" +
                "2. Log out\n" +
                "3. Subscribe to topic\n" +
                "4. Subscribe to keyword\n" +
                "5. Unsubscribe to topic\n" +
                "6. Unsubscribe to keyword\n" +
                "7. Unsubscribe all\n" +
                "8. List subscribed topics\n" +
                "9. EXIT\n";

        Scanner sc = new Scanner(System.in);
        String commandNum = "";

        while (!"9".equals(commandNum)) {
            System.out.println("\n========  Choose a command =========");
            System.out.println(commands);
            commandNum = sc.next();

            switch (commandNum) {
                case "1":
                    login();
                    break;
                case "2":
                    if (status == Status.ONLINE)
                        logout();
                    else System.out.println("Not logged in");
                    break;
                case "3":
                    if (status == Status.ONLINE)
                        subscribeHelperTopic(sc);
                    else System.out.println("Not logged in");
                    break;
                case "4":
                    if (status == Status.ONLINE)
                        subscribeHelperKeyword(sc);
                    else System.out.println("Not logged in");
                    break;
                case "5":
                    if (status == Status.ONLINE)
                        unsubscribeHelperTopic(sc);
                    else System.out.println("Not logged in");
                    break;
                case "6":
                    if (status == Status.ONLINE)
                        unsubscribeHelperKeyword(sc);
                    else System.out.println("Not logged in");
                    break;
                case "7":
                    if (status == Status.ONLINE)
                        unsubscribeAll();
                    else System.out.println("Not logged in");
                    break;
                case "8":
                    listSubscribed();
                    break;
                default:
                    break;
            }
        }

        System.out.println("Exiting program");
        System.exit(-2);
    }

    /**
     * Inserts messages into the message Queue.
     *
     * @param message received message
     */
    @Override
    public void insertIntoMessageQueue(Message message) {
        synchronized (lock) {
            this.messageQueue.offer(message);
            lock.notify();
        }
    }

    /**
     * Process messages from the message queue
     */
    @Override
    public void process() {
        synchronized (lock) {
            while (!messageQueue.isEmpty()) {
                Message message = messageQueue.poll();
                Type type = message.getType();

                switch (type) {
                    case NOTIFY_T:
                        handleNotificationTopic(message);
                        break;
                    case NOTIFY_E:
                        handleNotificationEvent(message);
                        break;
                    case NOTIFY_T_SUB:
                        handleNotificationTopicSub(message);
                        break;
                    case NOTIFY_K_SUB:
                        handleNotificationKeywordSub(message);
                        break;
                    default:
                        break;
                }

            }
            // wait for notification from the receiver thread
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
