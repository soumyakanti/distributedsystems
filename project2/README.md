## Run instructions

*  To run this project, go to the folder DistributedSystems, where there project specific Dockerfile, `Dockerfile-project2`, and docker-compose file, `docker-compose-project2.yml` are available.
*  Run the command `sudo docker-compose -f docker-compose-project2.yml -p project2 up -d --build --scale pub=2 --scale sub=2` to start 5 containers, 1 event manager and 2 each of publishers and subscribers. Change the `--scale` value as needed.
*  Attach to the event manager with `sudo docker exec -it project2_manager_1 bash` and start the event manager with `# ./run.sh`.
*  Attach to publishers using `sudo docker exec -it project2_pub_1 bash`, and run `# ./run.sh 1 P` to start publisher with id = 1.
*  Attach to subscribers using `sudo docker exec -it project2_sub_1 bash`, and run `# ./run.sh 1 S` to start subscriber with id = 1.
*  This info is also available in `instructions.txt`.
*  `run.sh` takes 2 arguments, `agent ID` and `agent type` (P/S).
*  Please keep the event manager running and there cannot be more than 1 event manager.