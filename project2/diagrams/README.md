This folder contains architecture and interaction diagrams for the pub sub
project.

The diagrams are in ``.svg`` instead of ``.pdf`` because it was difficult to 
convert them into ``.pdf``.

Please open them in a browser after downloading them, for best results.

I have divided interactions into 4 diagrams, and each one of them only contains 
the data structure needed. For an overview of the whole program, read the text
below the diagram in architecture.svg