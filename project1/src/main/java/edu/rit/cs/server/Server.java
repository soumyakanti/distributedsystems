/**
 * @author: Soumyakanti Das
 */

package edu.rit.cs.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This class serves as the server for computing word count.
 */
public class Server {
    public static void main(String args[]) {
        try {
            int serverPort = 7896;
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("TCP Server is running and accepting client connections...");
            while (true) {
                Socket clientSocket = listenSocket.accept();
                Connection c = new Connection(clientSocket);
            }
        } catch (IOException e) {
            System.out.println("Listen :" + e.getMessage());
        }
    }
}
