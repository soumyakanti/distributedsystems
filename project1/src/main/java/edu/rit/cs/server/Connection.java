/**
 * @author: Soumyakanti Das
 *
 */

package edu.rit.cs.server;

import edu.rit.cs.basic_word_count.AmazonFineFoodReview;
import edu.rit.cs.basic_word_count.KV;
import static edu.rit.cs.basic_word_count.WordCount_Seq_Improved.*;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Map;

/**
 * This class is used to handle an incoming connection.
 * It takes the socket as input, reads the incoming list,
 * and uses map and reduce methods to compute the word count.
 *
 */
public class Connection extends Thread {
    ObjectInputStream in;
    ObjectOutputStream out;
    Socket clientSocket;

    public Connection(Socket aClientSocket) {
        try {
            clientSocket = aClientSocket;
            out = new ObjectOutputStream(clientSocket.getOutputStream());
            in = new ObjectInputStream(clientSocket.getInputStream());

            // start thread
            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    public void run() {
        try {
            // read data, then compute word count with map and reduce methods.
            List<AmazonFineFoodReview> data = (List<AmazonFineFoodReview>) in.readObject();
            List<KV<String, Integer>> kv_pairs = map(data);
            Map<String, Integer> results = reduce(kv_pairs);

            out.writeObject(results);
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("IO:" + e.getMessage());
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {/*close failed*/}
        }
    }
}
