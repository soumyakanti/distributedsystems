/**
 * @author: Soumyakanti Das
 */
package edu.rit.cs.client;

import edu.rit.cs.basic_word_count.AmazonFineFoodReview;
import edu.rit.cs.basic_word_count.WordCount_Seq_Improved;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

/**
 * Client takes the filepath and IP of the server as arguments,
 * and sends the file across to the server for computing
 * word count.
 */
public class Client {
    public static void main(String args[]) {
        // arguments supply message and hostname of destination
        String fileName = args[0];
        System.out.println(fileName);
        String server_address = args[1];

        // read in the file and generate List of AmazonFineFoodReviews
        List<AmazonFineFoodReview> reviews = WordCount_Seq_Improved.read_reviews(fileName);
//        for (AmazonFineFoodReview a: reviews) System.out.println(a.get_Summary());

        Socket s = null;
        try {
            int serverPort = 7896;
            s = new Socket(server_address, serverPort);
            ObjectOutputStream out =
                    new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(s.getInputStream());
            //send the file to the server
            out.writeObject(reviews);
            System.out.println("Sent: reviews");
            // get data from the server
            Map<String, Integer> data = (Map<String, Integer>)in.readObject();

            for (String key: data.keySet()) {
                System.out.println(key + ": " + data.get(key));
            }
        } catch (UnknownHostException e) {
            System.out.println("Sock:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (s != null)
                try {
                    s.close();
                } catch (IOException e) {
                    System.out.println("close:" + e.getMessage());
                }
        }
    }
}
