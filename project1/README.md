## Description
This module uses the **basic_word_count** module provided by professor to count
words in affr.csv. 

## Instructions

To run this project, go to the parent module **distributedsystems**
and run `mvn package` to build all projects. Then, navigate to current 
folder **project1**, and run `sudo docker-compose up` to run the 
server and client programmes in attached mode.